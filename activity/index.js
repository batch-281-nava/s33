//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
/*  async function getSingleToDo() {
  return await //add fetch here.

  fetch("https://jsonplaceholder.typicode.com/todos/1")
    .then((response) => response.json())
    .then((json) => json);
}
*/

// Getting all to do list item
async function getAllToDo() {
  return await fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((json) => (titles = json.map((item) => item.title)));
}

getAllToDo().then((titles) => console.log(titles));

// [Section] Getting a specific to do list item
async function getSpecificToDo() {
  return await fetch("https://jsonplaceholder.typicode.com/posts/1")
    .then((response) => response.json())
    .then((json) => console.log(json));
}
getSpecificToDo();
// [Section] Creating a to do list item using POST method
async function createToDo() {
  return await fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      completed: false,
      title: "Created To Do LIst Item",
      userId: 1,
    }),
  })
    .then((response) => response.json())
    .then((json) => console.log(json));
}
createToDo();
// [Section] Updating a to do list item using PUT method
async function updateToDo() {
  return await fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      description:
        "To update the my to do list with a different data structure",
      status: "Pending",
      title: "Updated To Do List Item",
    }),
  })
    .then((response) => response.json())
    .then((json) => console.log(json));
}
updateToDo();
// [Section] Deleting a to do list item
async function deleteToDo() {
  return await fetch("https://jsonplaceholder.typicode.com/posts/1", {
    method: "DELETE",
  });
}